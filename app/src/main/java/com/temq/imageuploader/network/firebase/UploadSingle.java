package com.temq.imageuploader.network.firebase;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.UploadTask;
import com.temq.imageuploader.utils.BaseDisposable;

import io.reactivex.Single;
import io.reactivex.SingleObserver;

public class UploadSingle extends Single<UploadTask.TaskSnapshot> {

    private final Task<UploadTask.TaskSnapshot> uploadTask;

    UploadSingle(Task<UploadTask.TaskSnapshot> uploadTask) {
        this.uploadTask = uploadTask;
    }

    @Override
    protected void subscribeActual(SingleObserver<? super UploadTask.TaskSnapshot> observer) {
        UploadResultListener uploadResultListener = new UploadResultListener(observer);
        observer.onSubscribe(uploadResultListener);
        uploadTask.addOnFailureListener(uploadResultListener)
                .addOnSuccessListener(uploadResultListener);
    }

    static final class UploadResultListener extends BaseDisposable implements OnFailureListener,
            OnSuccessListener<UploadTask.TaskSnapshot> {

        private final SingleObserver<? super UploadTask.TaskSnapshot> observer;

        UploadResultListener(SingleObserver<? super UploadTask.TaskSnapshot> observer) {
            this.observer = observer;
        }

        @Override
        public void onFailure(@NonNull Exception e) {
            if (!isDisposed()) {
                observer.onError(e);
                dispose();
            }
        }

        @Override
        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
            if (isDisposed()) {
                return;
            }
            dispose();

            try {

                Exception error = taskSnapshot.getError();
                if (error != null) {
                    observer.onError(error);
                    return;
                }

                observer.onSuccess(taskSnapshot);

            } catch (Exception e) {
                observer.onError(e);
            }
        }
    }
}
