package com.temq.imageuploader.network.firebase;

import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.temq.imageuploader.utils.BaseDisposable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.SingleObserver;

public class FileItemsSingle extends Single<List<FirebaseFileItem>> {

    private final DatabaseReference databaseReference;

    FileItemsSingle(DatabaseReference databaseReference) {
        this.databaseReference = databaseReference;
    }


    @Override
    protected void subscribeActual(SingleObserver<? super List<FirebaseFileItem>> observer) {
        FileItemListener listener = new FileItemListener(observer);
        observer.onSubscribe(listener);
        databaseReference.addListenerForSingleValueEvent(listener);
    }

    static class FileItemListener extends BaseDisposable implements ValueEventListener {

        private final SingleObserver<? super List<FirebaseFileItem>> observer;


        FileItemListener(SingleObserver<? super List<FirebaseFileItem>> observer) {
            this.observer = observer;
        }

        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

            if (isDisposed()) {
                return;
            }

            long count = dataSnapshot.getChildrenCount();
            final List<FirebaseFileItem> items;
            if (count <= 0) {
                items = Collections.emptyList();
            } else {
                final int size = count > Integer.MAX_VALUE ? Integer.MAX_VALUE : (int) count;
                items = new ArrayList<>(size);
                int currentCount = 0;
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    FirebaseFileItem item = child.getValue(FirebaseFileItem.class);
                    items.add(item);
                    if (++currentCount == Integer.MAX_VALUE) break;
                }
            }

            observer.onSuccess(items);
            dispose();
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
            if (isDisposed()) {
                return;
            }

            observer.onError(databaseError.toException());
            dispose();
        }
    }
}
