package com.temq.imageuploader.network.firebase;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.OpenableColumns;

import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.temq.imageuploader.model.FileItem;
import com.temq.imageuploader.network.FilesStorage;
import com.temq.imageuploader.network.Progress;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.Single;

public class FirebaseFilesStorage implements FilesStorage {

    static final String USERS_PREFIX = "users";

    private static final String FILES_PATH = "files/";

    private final Context context;
    private final FirebaseStorage firebaseStorage;
    private final FirebaseDatabase firebaseDatabase;
    private final FirebaseAuth firebaseAuth;


    @Inject
    public FirebaseFilesStorage(Context context, FirebaseStorage firebaseStorage,
                                FirebaseDatabase firebaseDatabase,
                                FirebaseAuth firebaseAuth) {
        this.context = context;
        this.firebaseStorage = firebaseStorage;
        this.firebaseDatabase = firebaseDatabase;
        this.firebaseAuth = firebaseAuth;
    }

    @Override
    public Single<List<FileItem>> loadFiles() {
        return getUserReference()
                .flatMap(databaseReference -> new FileItemsSingle(databaseReference.child("files")).toMaybe())
                .map(firebaseItems -> {
                    List<FileItem> items = new ArrayList<>(firebaseItems.size());
                    for (FirebaseFileItem item : firebaseItems) {
                        items.add(new FileItem(item.getName(),
                                item.getPath(),
                                item.getSizeBytes(),
                                new Date(item.getDate())));
                    }
                    return items;
                })
                .toSingle(Collections.emptyList());
    }

    @Override
    public Single<FileItem> uploadFile(Uri uri) {
        return Single.fromCallable(() -> createUploadFileStorage(uri))
                .flatMap(storageReference -> {
                    UploadTask task = storageReference.putFile(uri);
                    return new UploadSingle(task);
                })
                .map(taskSnapshot -> {
                    StorageMetadata metadata = taskSnapshot.getMetadata();

                    if (metadata == null) {
                        throw new NullPointerException("Metadata is null");
                    }

                    return new FileItem(metadata.getName(),
                            metadata.getPath(),
                            metadata.getSizeBytes(),
                            new Date(metadata.getUpdatedTimeMillis()));
                })
                .flatMap(fileItem -> saveInfoAboutFile(fileItem).toSingleDefault(fileItem));
    }

    @Override
    public Observable<Progress> downloadFile(FileItem fileItem) {
        return Single.fromCallable(() -> createLocalFileUri(fileItem.getName()))
                .flatMapObservable(uri -> {
                    FileDownloadTask task = createDownloadFileStorage(fileItem, uri);
                    return new FileDowloadObservable(task);
                });
    }

    private Completable saveInfoAboutFile(FileItem fileItem) {
        return getUserReference()
                .flatMapCompletable(databaseReference -> {
                    FirebaseFileItem item = new FirebaseFileItem(fileItem);
                    Task<Void> task = databaseReference.child("files").push().setValue(item);
                    return new VoidTaskCompletable(task);
                });
    }

    private StorageReference createUploadFileStorage(Uri uri) {
        StorageReference storageReference = firebaseStorage.getReference();
        return storageReference.child(FILES_PATH + getFileName(uri));
    }

    private String getFileName(Uri uri) {
        String result = null;
        if ("content".equals(uri.getScheme())) {
            Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    private Uri createLocalFileUri(String fileName) throws Exception {
        File downloadFolder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        File file = new File(downloadFolder, fileName);

        int counter = 1;
        while (file.exists()) {
            file = new File(downloadFolder, String.format(Locale.getDefault(),
                    "(%d) %s", counter++, fileName));
        }

        if (!file.createNewFile()) {
            throw new Exception("File wasn't created");
        }
        return Uri.fromFile(file);
    }

    private FileDownloadTask createDownloadFileStorage(FileItem fileItem, Uri localFileUri) {
        StorageReference child = firebaseStorage.getReference().child(fileItem.getPath());
        return child.getFile(localFileUri);
    }

    private Maybe<DatabaseReference> getUserReference() {
        return Maybe
                .defer(() -> {
                    FirebaseUser currentUser = firebaseAuth.getCurrentUser();
                    if (currentUser == null) {
                        return Maybe.empty();
                    }
                    return Maybe.just(currentUser.getUid());
                })
                .map(uid -> firebaseDatabase.getReference().child(USERS_PREFIX).child(uid));
    }
}
