package com.temq.imageuploader.network;

import android.net.Uri;

import com.temq.imageuploader.model.FileItem;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

public interface FilesStorage {

    Single<List<FileItem>> loadFiles();

    Single<FileItem> uploadFile(Uri uri);

    Observable<Progress> downloadFile(FileItem fileItem);

}
