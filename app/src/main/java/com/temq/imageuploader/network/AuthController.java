package com.temq.imageuploader.network;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface AuthController {

    Completable auth(String login, String password);

    Completable register(String name, String email, String phone, String password);

    Completable logout();

    Completable resetPassword(String email);

    Single<Boolean> checkVerify();
}
