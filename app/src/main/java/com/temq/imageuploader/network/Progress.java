package com.temq.imageuploader.network;

public final class Progress {

    private final long totalBytes;
    private final long current;

    public Progress(long totalBytes, long transferredBytes) {
        this.totalBytes = totalBytes;
        this.current = transferredBytes;
    }

    public long getTotalBytes() {
        return totalBytes;
    }

    public long getCurrent() {
        return current;
    }
}
