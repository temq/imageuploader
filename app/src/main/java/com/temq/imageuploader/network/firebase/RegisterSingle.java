package com.temq.imageuploader.network.firebase;

import com.google.firebase.auth.AuthResult;

import io.reactivex.Single;
import io.reactivex.SingleObserver;

public class RegisterSingle extends Single<AuthResult> {
    @Override
    protected void subscribeActual(SingleObserver<? super AuthResult> observer) {

    }
}
