package com.temq.imageuploader.network.firebase;

import com.google.firebase.database.IgnoreExtraProperties;
import com.temq.imageuploader.model.FileItem;

@IgnoreExtraProperties
public class FirebaseFileItem {

    private String name;
    private String path;
    private long sizeBytes;
    private long date;

    public FirebaseFileItem() {
    }

    public FirebaseFileItem(FileItem fileItem) {
        this(fileItem.getName(), fileItem.getPath(), fileItem.getSizeBytes(), fileItem.getDate().getTime());
    }

    public FirebaseFileItem(String name, String path, long sizeBytes, long date) {
        this.name = name;
        this.path = path;
        this.sizeBytes = sizeBytes;
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public long getSizeBytes() {
        return sizeBytes;
    }

    public void setSizeBytes(long sizeBytes) {
        this.sizeBytes = sizeBytes;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }
}
