package com.temq.imageuploader.network.firebase;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;

import java.util.concurrent.atomic.AtomicBoolean;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;

class AuthSingle extends Single<AuthResult> {

    private final Task<AuthResult> authResultTask;

    AuthSingle(Task<AuthResult> authResultTask) {
        this.authResultTask = authResultTask;
    }

    @Override
    protected void subscribeActual(SingleObserver<? super AuthResult> observer) {
        AuthCompletableListener listener = new AuthCompletableListener(observer);
        observer.onSubscribe(listener);
        authResultTask.addOnCompleteListener(listener)
                .addOnFailureListener(listener);
    }

    static final class AuthCompletableListener implements Disposable,
            OnCompleteListener<AuthResult>,
            OnFailureListener {

        private final SingleObserver<? super AuthResult> observer;
        private final AtomicBoolean isDisposed;

        AuthCompletableListener(SingleObserver<? super AuthResult> observer) {
            this.observer = observer;
            isDisposed = new AtomicBoolean(false);
        }

        @Override
        public void onComplete(@NonNull Task<AuthResult> task) {

            if (isDisposed.get()) {
                return;
            }

            isDisposed.set(true);

            try {

                Exception exception = task.getException();
                if (exception != null) {
                    observer.onError(exception);
                    return;
                }

                AuthResult result = task.getResult();
                if (result == null) {
                    observer.onError(new IllegalStateException("Task result is null"));
                    return;
                }
                observer.onSuccess(result);
            } catch (Exception e) {
                observer.onError(e);
            }
        }

        @Override
        public void dispose() {
            isDisposed.set(true);
        }

        @Override
        public boolean isDisposed() {
            return isDisposed.get();
        }

        @Override
        public void onFailure(@NonNull Exception e) {
            if (!isDisposed.get()) {
                observer.onError(e);
                isDisposed.set(true);
            }
        }
    }
}
