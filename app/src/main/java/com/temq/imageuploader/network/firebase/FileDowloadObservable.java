package com.temq.imageuploader.network.firebase;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.OnProgressListener;
import com.temq.imageuploader.network.Progress;
import com.temq.imageuploader.utils.BaseDisposable;

import io.reactivex.Observable;
import io.reactivex.Observer;

public class FileDowloadObservable extends Observable<Progress> {

    private final FileDownloadTask task;

    FileDowloadObservable(FileDownloadTask task) {
        this.task = task;
    }

    @Override
    protected void subscribeActual(Observer<? super Progress> observer) {
        FileDownloadListener fileDownloadListener = new FileDownloadListener(observer);
        observer.onSubscribe(fileDownloadListener);
        task.addOnSuccessListener(fileDownloadListener)
                .addOnProgressListener(fileDownloadListener)
                .addOnFailureListener(fileDownloadListener);
    }

    static final class FileDownloadListener extends BaseDisposable implements

            OnSuccessListener<FileDownloadTask.TaskSnapshot>,
            OnFailureListener,
            OnProgressListener<FileDownloadTask.TaskSnapshot> {

        private final Observer<? super Progress> observer;

        FileDownloadListener(Observer<? super Progress> observer) {
            this.observer = observer;
        }

        @Override
        public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
            if (isDisposed()) {
                return;
            }

            dispose();

            try {
                observer.onComplete();
            } catch (Exception e) {
                observer.onError(e);
            }
        }

        @Override
        public void onFailure(@NonNull Exception e) {
            if (isDisposed()) {
                return;
            }
            observer.onError(e);
            dispose();
        }

        @Override
        public void onProgress(FileDownloadTask.TaskSnapshot taskSnapshot) {
            if (!isDisposed()) {
                observer.onNext(new Progress(taskSnapshot.getTotalByteCount(),
                        taskSnapshot.getBytesTransferred()));
            }
        }
    }
}
