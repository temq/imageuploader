package com.temq.imageuploader.network.firebase;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.temq.imageuploader.utils.BaseDisposable;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;

public class VoidTaskCompletable extends Completable {

    private final Task<Void> task;

    VoidTaskCompletable(Task<Void> task) {
        this.task = task;
    }

    @Override
    protected void subscribeActual(CompletableObserver s) {
        TaskListener listener = new TaskListener(s);
        s.onSubscribe(listener);
        task.addOnSuccessListener(listener)
                .addOnFailureListener(listener);
    }

    static class TaskListener extends BaseDisposable implements OnSuccessListener<Void>, OnFailureListener {
        private final CompletableObserver observer;

        TaskListener(CompletableObserver observer) {
            this.observer = observer;
        }

        @Override
        public void onSuccess(Void aVoid) {
            if (isDisposed()) {
                return;
            }

            observer.onComplete();
            dispose();
        }

        @Override
        public void onFailure(@NonNull Exception e) {
            if (isDisposed()) {
                return;
            }
            observer.onError(e);
            dispose();
        }
    }
}
