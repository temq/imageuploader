package com.temq.imageuploader.network.firebase;

import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.temq.imageuploader.network.AuthController;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Single;

import static com.temq.imageuploader.network.firebase.FirebaseFilesStorage.USERS_PREFIX;

public class FirebaseAuthController implements AuthController {

    private final FirebaseAuth firebaseAuth;
    private final FirebaseDatabase firebaseDatabase;

    @Inject
    public FirebaseAuthController(FirebaseAuth firebaseAuth, FirebaseDatabase firebaseDatabase) {
        this.firebaseAuth = firebaseAuth;
        this.firebaseDatabase = firebaseDatabase;
    }

    @Override
    public Completable auth(String login, String password) {
        Task<AuthResult> authResultTask = firebaseAuth.signInWithEmailAndPassword(login, password);
        return new AuthSingle(authResultTask).ignoreElement();
    }

    @Override
    public Completable register(String name, String email, String phone, String password) {
        Task<AuthResult> task = firebaseAuth.createUserWithEmailAndPassword(email, password);
        return new AuthSingle(task)
                .flatMapCompletable(authResult -> {
                    Task<Void> verificationTask = authResult.getUser().sendEmailVerification();
                    return new VoidTaskCompletable(verificationTask);
                })
                .andThen(saveUserData(name, email, phone));
    }

    @Override
    public Completable logout() {
        return Completable.fromAction(firebaseAuth::signOut);
    }

    @Override
    public Completable resetPassword(String email) {
        Task<Void> task = firebaseAuth.sendPasswordResetEmail(email);
        return new VoidTaskCompletable(task);
    }

    @Override
    public Single<Boolean> checkVerify() {
        return Maybe.fromCallable(firebaseAuth::getCurrentUser)
                .flatMap(firebaseUser -> {
                    Task<Void> reload = firebaseUser.reload();
                    return new VoidTaskCompletable(reload)
                            .andThen(Maybe.fromCallable(firebaseAuth::getCurrentUser));
                })
                .flatMap(currentUser -> Maybe.fromCallable(currentUser::isEmailVerified))
                .toSingle(false);
    }

    private Completable saveUserData(String name, String email, String phone) {
        return getUserReference()
                .flatMapCompletable(databaseReference -> {
                    Task<Void> task = databaseReference.setValue(new FirebaseAuthUser(email, phone, name));
                    return new VoidTaskCompletable(task);
                });
    }

    private Maybe<DatabaseReference> getUserReference() {
        return Maybe
                .defer(() -> {
                    FirebaseUser currentUser = firebaseAuth.getCurrentUser();
                    if (currentUser == null) {
                        return Maybe.empty();
                    }
                    return Maybe.just(currentUser.getUid());
                })
                .map(uid -> firebaseDatabase.getReference().child(USERS_PREFIX).child(uid));
    }
}
