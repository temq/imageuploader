package com.temq.imageuploader;

import android.app.Application;
import android.support.v7.app.AppCompatDelegate;

import com.google.firebase.FirebaseApp;
import com.temq.imageuploader.dagger.AppComponent;
import com.temq.imageuploader.dagger.DaggerAppComponent;

public class App extends Application {

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseApp.initializeApp(this);
        appComponent = DaggerAppComponent.builder().application(this).build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
