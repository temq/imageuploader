package com.temq.imageuploader.dagger;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
class FirebaseModule {

    @Provides
    @Singleton
    static FirebaseAuth firebaseAuth() {
        return FirebaseAuth.getInstance();
    }

    @Provides
    @Singleton
    static FirebaseStorage firebaseStorage() {
        return FirebaseStorage.getInstance();
    }

    @Provides
    @Singleton
    static FirebaseDatabase firebaseDatabase() {
        return FirebaseDatabase.getInstance();
    }

}
