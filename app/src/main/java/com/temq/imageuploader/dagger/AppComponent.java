package com.temq.imageuploader.dagger;

import com.temq.imageuploader.App;
import com.temq.imageuploader.ui.auth.AuthComponent;
import com.temq.imageuploader.ui.photo.FilesComponent;
import com.temq.imageuploader.ui.signup.SignUpComponent;
import com.temq.imageuploader.ui.resetpass.ResetPasswordComponent;
import com.temq.imageuploader.ui.splash.SplashActivity;
import com.temq.imageuploader.ui.verify.VerifyComponent;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;

@Component(modules = {
        AppModule.class,
        FirebaseModule.class,
        NetworkModule.class})
@Singleton
public interface AppComponent {

    AuthComponent.Bulder authComponentBuilder();

    SignUpComponent.Builder registerComponentBuilder();

    FilesComponent.Builder filesComponentBuilder();

    ResetPasswordComponent.Builder resetComponentBuilder();

    VerifyComponent.Builder verifyComponentBuilder();

    void inject(SplashActivity splashActivity);

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(App app);

        AppComponent build();
    }
}
