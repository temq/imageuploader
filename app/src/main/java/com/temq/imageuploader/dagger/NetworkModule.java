package com.temq.imageuploader.dagger;

import com.temq.imageuploader.network.AuthController;
import com.temq.imageuploader.network.FilesStorage;
import com.temq.imageuploader.network.firebase.FirebaseAuthController;
import com.temq.imageuploader.network.firebase.FirebaseFilesStorage;

import dagger.Binds;
import dagger.Module;

@Module
public interface NetworkModule {

    @Binds
    AuthController authController(FirebaseAuthController controller);

    @Binds
    FilesStorage fileStorage(FirebaseFilesStorage firebaseStorage);

}
