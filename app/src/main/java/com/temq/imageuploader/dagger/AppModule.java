package com.temq.imageuploader.dagger;

import android.content.Context;

import com.temq.imageuploader.App;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    @Provides
    @Singleton
    static Context context(App app) {
        return app;
    }
}
