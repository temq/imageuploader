package com.temq.imageuploader.model;

public class User {
    private final String email;
    private final String phone;
    private final String name;

    public User(String email, String phone, String name) {
        this.email = email;
        this.phone = phone;
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getName() {
        return name;
    }
}
