package com.temq.imageuploader.model;

import java.util.Date;

public class FileItem {

    private final String name;
    private final String path;
    private final long sizeBytes;
    private final Date date;

    public FileItem(String name, String path, long sizeBytes, Date date) {
        this.name = name;
        this.path = path;
        this.sizeBytes = sizeBytes;
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public String getPath() {
        return path;
    }

    public long getSizeBytes() {
        return sizeBytes;
    }

    public Date getDate() {
        return date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FileItem fileItem = (FileItem) o;

        if (sizeBytes != fileItem.sizeBytes) return false;
        if (!name.equals(fileItem.name)) return false;
        if (!path.equals(fileItem.path)) return false;
        return date.equals(fileItem.date);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + path.hashCode();
        result = 31 * result + (int) (sizeBytes ^ (sizeBytes >>> 32));
        result = 31 * result + date.hashCode();
        return result;
    }
}
