package com.temq.imageuploader.utils;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class AccountHelper {

    private final FirebaseAuth firebaseAuth;

    @Inject
    public AccountHelper(FirebaseAuth firebaseAuth) {
        this.firebaseAuth = firebaseAuth;
    }

    public boolean isEmailVerified() {
        FirebaseUser currentUser = firebaseAuth.getCurrentUser();
        return currentUser != null && currentUser.isEmailVerified();
    }

    public boolean isAuth() {
        return firebaseAuth.getCurrentUser() != null;
    }

}
