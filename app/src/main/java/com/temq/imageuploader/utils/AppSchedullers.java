package com.temq.imageuploader.utils;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@Singleton
public class AppSchedullers {

    @Inject
    public AppSchedullers() {
    }

    public Scheduler network() {
        return Schedulers.computation();
    }

    public Scheduler background() {
        return Schedulers.io();
    }

    public Scheduler ui() {
        return AndroidSchedulers.mainThread();
    }
}
