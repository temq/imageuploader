package com.temq.imageuploader.utils;

import java.util.concurrent.atomic.AtomicBoolean;

import io.reactivex.disposables.Disposable;

public abstract class BaseDisposable implements Disposable {

    private final AtomicBoolean isDisposed = new AtomicBoolean();

    @Override
    public void dispose() {
        isDisposed.set(true);
    }

    @Override
    public boolean isDisposed() {
        return isDisposed.get();
    }
}
