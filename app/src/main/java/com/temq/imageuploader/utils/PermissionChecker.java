package com.temq.imageuploader.utils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;

import com.temq.imageuploader.App;

import javax.inject.Inject;

public class PermissionChecker {

    private final Context context;

    @Inject
    public PermissionChecker(App context) {
        this.context = context;
    }

    public boolean isGranted(String permission) {
        int result = ContextCompat.checkSelfPermission(context, permission);
        return result == PackageManager.PERMISSION_GRANTED;
    }

}
