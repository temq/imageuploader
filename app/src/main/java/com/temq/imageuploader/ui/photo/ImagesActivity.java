package com.temq.imageuploader.ui.photo;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.temq.imageuploader.R;
import com.temq.imageuploader.model.FileItem;
import com.temq.imageuploader.ui.BaseActivity;
import com.temq.imageuploader.ui.auth.AuthActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ImagesActivity extends BaseActivity implements FileView {

    private static final int REQUEST_PICK_IMAGE = 1010;

    private static final int REQUEST_WRITE_PERMISSION = 2001;

    @BindView(R.id.file_list)
    RecyclerView fileList;
    @BindView(R.id.no_files_stub)
    View noFilesStub;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @InjectPresenter
    FilesPresenter presenter;

    private FileAdapter adapter;

    public static Intent createIntent(Context context) {
        return new Intent(context, ImagesActivity.class);
    }

    @ProvidePresenter
    FilesPresenter presenter() {
        return appComponent().filesComponentBuilder().build().presenter();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_files);
        ButterKnife.bind(this);

        fileList.setLayoutManager(new LinearLayoutManager(this));
        adapter = new FileAdapter(this, getLayoutInflater());
        adapter.setListener(fileItem -> presenter.startDownload(fileItem));
        fileList.setAdapter(adapter);

        toolbar.inflateMenu(R.menu.menu_files_screen);
        toolbar.setTitle(R.string.files_title);
        toolbar.setOnMenuItemClickListener(menuItem -> {
            switch (menuItem.getItemId()) {
                case R.id.logout:
                    presenter.logout();
                    return true;
            }
            return false;
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) return;

        switch (requestCode) {
            case REQUEST_PICK_IMAGE:
                presenter.onFileSelected(data.getData());
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (REQUEST_WRITE_PERMISSION == requestCode) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                presenter.permissionGranted();
            }
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void showFiles(List<FileItem> files) {
        noFilesStub.setVisibility(files.isEmpty() ? View.VISIBLE : View.GONE);
        adapter.submitList(files);
    }

    @Override
    public void openSelectFile() {
        Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
        getIntent.setType("*/*");

        Intent pickIntent = new Intent(Intent.ACTION_PICK);
        pickIntent.setDataAndType(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "*/*");

        Intent chooserIntent = Intent.createChooser(getIntent, "Select file");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{pickIntent});

        startActivityForResult(chooserIntent, REQUEST_PICK_IMAGE);
    }

    @Override
    public void openAuthScreen() {
        Intent intent = AuthActivity.createIntent(this);
        startActivity(intent);
        finish();
    }

    @Override
    public void showError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showUnknownLogoutError() {
        Toast.makeText(this, R.string.unknown_error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void errorFileLoading() {
        Toast.makeText(this, R.string.loading_fail, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoadSuccess() {
        Toast.makeText(this, R.string.loading_ok, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void requestPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                REQUEST_WRITE_PERMISSION);
    }

    @OnClick(R.id.upload_file)
    void onUploadClick() {
        presenter.onSelectFileClick();
    }
}
