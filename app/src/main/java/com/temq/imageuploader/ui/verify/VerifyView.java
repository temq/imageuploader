package com.temq.imageuploader.ui.verify;

import com.temq.imageuploader.ui.BaseView;

public interface VerifyView extends BaseView {
    void openMainScreen();

    void emailNotVerified();
}
