package com.temq.imageuploader.ui.signup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.temq.imageuploader.R;
import com.temq.imageuploader.ui.BaseActivity;
import com.temq.imageuploader.ui.verify.VerifyActivity;

import androidx.annotation.StringRes;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignUpActivity extends BaseActivity implements SignUpView {

    @BindView(R.id.signUpName)
    TextInputLayout nameInputLayout;
    @BindView(R.id.signUpEmail)
    TextInputLayout emailInputLayout;
    @BindView(R.id.signUpPhone)
    TextInputLayout phoneInputLayout;
    @BindView(R.id.signUpPassword)
    TextInputLayout passwordInputLayout;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @InjectPresenter
    SignUpPresenter presenter;

    public static Intent createIntent(Context context) {
        return new Intent(context, SignUpActivity.class);
    }

    @ProvidePresenter
    SignUpPresenter presenter() {
        return appComponent().registerComponentBuilder().build().presenter();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        toolbar.setTitle(R.string.register_title);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());
    }

    @OnClick(R.id.signUp)
    void signUpClick() {
        presenter.onSignUpClick(
                getTextFromInputLayout(nameInputLayout),
                getTextFromInputLayout(emailInputLayout),
                getTextFromInputLayout(phoneInputLayout),
                getTextFromInputLayout(passwordInputLayout)
        );
    }

    @Override
    public void openMainScreen() {
        Intent intent = VerifyActivity.createIntent(this);
        startActivity(intent);
    }

    @Override
    public void showError() {
        Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showTextError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void clearErrors() {
        nameInputLayout.setError(null);
        emailInputLayout.setError(null);
        phoneInputLayout.setError(null);
        passwordInputLayout.setError(null);
    }

    @Override
    public void nameRequiredError() {
        showError(nameInputLayout, R.string.error_required_filed_default);
    }

    @Override
    public void emailRequiredError() {
        showError(emailInputLayout, R.string.error_required_filed_default);
    }

    @Override
    public void phoneRequired() {
        showError(phoneInputLayout, R.string.error_required_filed_default);
    }

    @Override
    public void passwordError() {
        showError(passwordInputLayout, R.string.error_required_filed_password);
    }

    private void showError(TextInputLayout textInputLayout, @StringRes int errorId) {
        textInputLayout.setError(getString(errorId));
    }
}
