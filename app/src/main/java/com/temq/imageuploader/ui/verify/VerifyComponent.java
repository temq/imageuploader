package com.temq.imageuploader.ui.verify;

import dagger.Subcomponent;

@Subcomponent
public interface VerifyComponent {

    VerifyPresenter presenter();

    @Subcomponent.Builder
    interface Builder {

        VerifyComponent build();
    }
}
