package com.temq.imageuploader.ui.signup;

import com.temq.imageuploader.ui.BaseView;

public interface SignUpView extends BaseView {
    void openMainScreen();

    void showError();

    void showTextError(String message);

    void clearErrors();

    void nameRequiredError();

    void emailRequiredError();

    void phoneRequired();

    void passwordError();
}
