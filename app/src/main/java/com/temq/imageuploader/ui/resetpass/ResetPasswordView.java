package com.temq.imageuploader.ui.resetpass;

import com.temq.imageuploader.ui.BaseView;

public interface ResetPasswordView extends BaseView {

    void onResetSuccess();

    void onResetFail();

    void emailEmptyError();

    void showTextError(String message);
}
