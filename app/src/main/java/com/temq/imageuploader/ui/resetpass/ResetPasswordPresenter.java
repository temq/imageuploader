package com.temq.imageuploader.ui.resetpass;

import android.text.TextUtils;

import com.arellomobile.mvp.InjectViewState;
import com.google.firebase.FirebaseException;
import com.temq.imageuploader.network.AuthController;
import com.temq.imageuploader.ui.BasePresenter;
import com.temq.imageuploader.utils.AppSchedullers;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;

@InjectViewState
public class ResetPasswordPresenter extends BasePresenter<ResetPasswordView> {

    private final AuthController authController;
    private final AppSchedullers appSchedullers;

    @Inject
    public ResetPasswordPresenter(AuthController authController, AppSchedullers appSchedullers) {
        this.authController = authController;
        this.appSchedullers = appSchedullers;
    }

    public void onResetClick(String email) {
        if (TextUtils.isEmpty(email)) {
            getViewState().emailEmptyError();
            return;
        }

        Disposable disposable = authController.resetPassword(email.trim())
                .subscribeOn(appSchedullers.network())
                .observeOn(appSchedullers.ui())
                .subscribe(() -> getViewState().onResetSuccess(),
                        throwable -> {
                            if (throwable instanceof FirebaseException) {
                                getViewState().showTextError(throwable.getMessage());
                            } else {
                                getViewState().onResetFail();
                            }
                        });
        add(disposable);
    }
}
