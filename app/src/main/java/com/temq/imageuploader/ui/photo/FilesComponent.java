package com.temq.imageuploader.ui.photo;

import dagger.Subcomponent;

@Subcomponent
public interface FilesComponent {

    FilesPresenter presenter();

    @Subcomponent.Builder
    interface Builder{
        FilesComponent build();
    }
}
