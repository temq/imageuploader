package com.temq.imageuploader.ui;

import android.support.design.widget.TextInputLayout;
import android.widget.EditText;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.temq.imageuploader.App;
import com.temq.imageuploader.dagger.AppComponent;

public abstract class BaseActivity extends MvpAppCompatActivity {

    protected AppComponent appComponent() {
        return ((App) getApplication()).getAppComponent();
    }

    protected String getTextFromInputLayout(TextInputLayout textInputLayout) {
        EditText editText = textInputLayout.getEditText();
        return editText == null ? "" : editText.getText().toString();
    }
}
