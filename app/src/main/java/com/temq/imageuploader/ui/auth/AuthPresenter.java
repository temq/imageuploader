package com.temq.imageuploader.ui.auth;

import com.arellomobile.mvp.InjectViewState;
import com.temq.imageuploader.network.AuthController;
import com.temq.imageuploader.ui.BasePresenter;
import com.temq.imageuploader.utils.AppSchedullers;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;

@InjectViewState
public class AuthPresenter extends BasePresenter<AuthView> {

    private final AuthController authController;
    private final AppSchedullers appSchedullers;

    @Inject
    public AuthPresenter(AuthController authController, AppSchedullers appSchedullers) {
        this.authController = authController;
        this.appSchedullers = appSchedullers;
    }

    void onSignIn(String login, String password) {
        Disposable disposable = authController.auth(login, password)
                .subscribeOn(appSchedullers.network())
                .observeOn(appSchedullers.ui())
                .subscribe(() -> getViewState().startMainScreen(),
                        throwable -> {
                            throwable.printStackTrace();
                            getViewState().showAuthError();
                        });
        add(disposable);
    }
}
