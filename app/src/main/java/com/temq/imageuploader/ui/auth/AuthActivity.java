package com.temq.imageuploader.ui.auth;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.temq.imageuploader.R;
import com.temq.imageuploader.ui.BaseActivity;
import com.temq.imageuploader.ui.photo.ImagesActivity;
import com.temq.imageuploader.ui.signup.SignUpActivity;
import com.temq.imageuploader.ui.resetpass.ResetPasswordActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AuthActivity extends BaseActivity implements AuthView {

    @BindView(R.id.loginView)
    TextInputLayout loginLayout;
    @BindView(R.id.password)
    TextInputLayout passwordLayout;

    @InjectPresenter
    AuthPresenter authPresenter;

    public static Intent createIntent(Context context) {
        return new Intent(context, AuthActivity.class);
    }

    @ProvidePresenter
    AuthPresenter presenter() {
        return appComponent().authComponentBuilder().build().presenter();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.signUp)
    void signUpClick() {
        startActivity(SignUpActivity.createIntent(this));
    }

    @OnClick(R.id.signIn)
    void signInClick() {
        authPresenter.onSignIn(getTextFromInputLayout(loginLayout),
                getTextFromInputLayout(passwordLayout));
    }

    @OnClick(R.id.forgotPass)
    void onForgotPassClick() {
        startActivity(ResetPasswordActivity.createIntent(this));
    }

    @Override
    public void startMainScreen() {
        Intent intent = ImagesActivity.createIntent(this);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void showAuthError() {
        Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
    }
}
