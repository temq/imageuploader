package com.temq.imageuploader.ui.signup;

import dagger.Subcomponent;

@Subcomponent
public interface SignUpComponent {

    SignUpPresenter presenter();

    @Subcomponent.Builder
    interface Builder {
        SignUpComponent build();
    }
}
