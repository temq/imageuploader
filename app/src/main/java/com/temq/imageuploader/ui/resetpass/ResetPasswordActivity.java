package com.temq.imageuploader.ui.resetpass;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.temq.imageuploader.R;
import com.temq.imageuploader.ui.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ResetPasswordActivity extends BaseActivity implements ResetPasswordView {

    @BindView(R.id.resetEmail)
    TextInputLayout resetEmailLayout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @InjectPresenter
    ResetPasswordPresenter presenter;

    public static Intent createIntent(Context context) {
        return new Intent(context, ResetPasswordActivity.class);
    }

    @ProvidePresenter
    ResetPasswordPresenter presenter() {
        return appComponent().resetComponentBuilder().build().presenter();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        ButterKnife.bind(this);
        toolbar.setTitle(R.string.reset_password_title);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());
    }

    @Override
    public void onResetSuccess() {
        Toast.makeText(this, R.string.reset_success, Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void onResetFail() {
        Toast.makeText(this, R.string.unknown_error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void emailEmptyError() {
        resetEmailLayout.setError(getString(R.string.error_required_filed_default));
    }

    @Override
    public void showTextError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.reset)
    void onResetClick() {
        resetEmailLayout.setError(null);
        presenter.onResetClick(getTextFromInputLayout(resetEmailLayout));
    }
}
