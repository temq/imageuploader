package com.temq.imageuploader.ui.verify;

import com.arellomobile.mvp.InjectViewState;
import com.temq.imageuploader.network.AuthController;
import com.temq.imageuploader.ui.BasePresenter;
import com.temq.imageuploader.utils.AppSchedullers;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;

@InjectViewState
public class VerifyPresenter extends BasePresenter<VerifyView> {

    private final AuthController authController;
    private final AppSchedullers appSchedullers;
    private Disposable verifyDisposable;

    @Inject
    public VerifyPresenter(AuthController authController, AppSchedullers appSchedullers) {
        this.authController = authController;
        this.appSchedullers = appSchedullers;
        verifyDisposable = Disposables.disposed();
    }

    public void onContinueClick() {

        if (!verifyDisposable.isDisposed()) {
            return;
        }

        verifyDisposable = authController.checkVerify()
                .subscribeOn(appSchedullers.network())
                .observeOn(appSchedullers.ui())
                .subscribe(result -> {
                    if (result) {
                        getViewState().openMainScreen();
                    } else {
                        getViewState().emailNotVerified();
                    }
                });
        add(verifyDisposable);
    }
}
