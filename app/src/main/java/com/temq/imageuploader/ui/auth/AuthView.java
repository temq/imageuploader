package com.temq.imageuploader.ui.auth;

import com.temq.imageuploader.ui.BaseView;

public interface AuthView extends BaseView {
    void startMainScreen();

    void showAuthError();
}
