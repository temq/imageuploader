package com.temq.imageuploader.ui.photo;

import com.arellomobile.mvp.MvpView;
import com.temq.imageuploader.model.FileItem;

import java.util.List;

public interface FileView extends MvpView {

    void showFiles(List<FileItem> files);

    void openSelectFile();

    void openAuthScreen();

    void showError(String message);

    void showUnknownLogoutError();

    void errorFileLoading();

    void showLoadSuccess();

    void requestPermission();
}
