package com.temq.imageuploader.ui.photo;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.temq.imageuploader.R;
import com.temq.imageuploader.model.FileItem;

import java.text.DateFormat;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FileAdapter extends ListAdapter<FileItem, FileAdapter.ViewHolder> {

    private final Context context;
    private final LayoutInflater layoutInflater;
    private final DateFormat dateFormat;

    private DowloadClickListener listener;

    protected FileAdapter(Context context, LayoutInflater layoutInflater) {
        super(new Callback());
        this.context = context;
        this.layoutInflater = layoutInflater;
        dateFormat = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT, Locale.getDefault());
    }

    public void setListener(DowloadClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = layoutInflater.inflate(R.layout.item_file_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.bind(getItem(i));
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.fileName)
        TextView fileName;
        @BindView(R.id.fileDate)
        TextView fildeDate;
        @BindView(R.id.downloadButton)
        Button downloadButton;

        private FileItem current;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(FileItem fileItem) {
            current = fileItem;

            fileName.setText(fileItem.getName());
            fildeDate.setText(dateFormat.format(fileItem.getDate()));
            float size = fileItem.getSizeBytes() / (1024.f * 1024);
            downloadButton.setText(context.getString(R.string.download_mb, size));
        }

        @OnClick(R.id.downloadButton)
        void onDownloadClick() {
            if (current != null && listener != null) {
                listener.onDonwloadClick(current);
            }
        }
    }

    static class Callback extends DiffUtil.ItemCallback<FileItem> {

        @Override
        public boolean areItemsTheSame(@NonNull FileItem fileItem, @NonNull FileItem t1) {
            return fileItem.equals(t1);
        }

        @Override
        public boolean areContentsTheSame(@NonNull FileItem fileItem, @NonNull FileItem t1) {
            return true;
        }
    }

    public interface DowloadClickListener {

        void onDonwloadClick(FileItem fileItem);

    }
}
