package com.temq.imageuploader.ui.auth;

import dagger.Subcomponent;

@Subcomponent
public interface AuthComponent {

    AuthPresenter presenter();

    @Subcomponent.Builder
    interface Bulder {

        AuthComponent build();
    }
}
