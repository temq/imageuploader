package com.temq.imageuploader.ui.signup;

import android.text.TextUtils;

import com.arellomobile.mvp.InjectViewState;
import com.google.firebase.FirebaseException;
import com.temq.imageuploader.network.AuthController;
import com.temq.imageuploader.ui.BasePresenter;
import com.temq.imageuploader.utils.AppSchedullers;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;

@InjectViewState
public class SignUpPresenter extends BasePresenter<SignUpView> {

    private final AuthController authController;
    private final AppSchedullers appSchedullers;

    @Inject
    SignUpPresenter(AuthController authController, AppSchedullers appSchedullers) {
        this.authController = authController;
        this.appSchedullers = appSchedullers;
    }

    public void onSignUpClick(String name, String email, String phone, String password) {

        getViewState().clearErrors();

        if (!checkData(name, email, phone, password)) {
            return;
        }

        Disposable disposable = authController.register(name, email, phone, password)
                .subscribeOn(appSchedullers.network())
                .observeOn(appSchedullers.ui())
                .subscribe(() -> getViewState().openMainScreen(),
                        throwable -> {
                            throwable.printStackTrace();
                            if (throwable instanceof FirebaseException) {
                                getViewState().showTextError(throwable.getMessage());
                            } else {
                                getViewState().showError();
                            }
                        });
        add(disposable);
    }

    private boolean checkData(String name, String email, String phone, String password) {

        boolean res = true;

        if (TextUtils.isEmpty(name)) {
            getViewState().nameRequiredError();
            res = false;
        }

        if (TextUtils.isEmpty(email)) {
            getViewState().emailRequiredError();
            res = false;
        }

        if (TextUtils.isEmpty(phone)) {
            getViewState().phoneRequired();
            res = false;
        }

        if (TextUtils.isEmpty(password) || password.length() < 6) {
            getViewState().passwordError();
            res = false;
        }

        return res;
    }
}
