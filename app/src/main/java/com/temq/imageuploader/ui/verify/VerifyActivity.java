package com.temq.imageuploader.ui.verify;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.temq.imageuploader.R;
import com.temq.imageuploader.ui.BaseActivity;
import com.temq.imageuploader.ui.photo.ImagesActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class VerifyActivity extends BaseActivity implements VerifyView {

    @InjectPresenter
    VerifyPresenter presenter;

    public static Intent createIntent(Context context) {
        return new Intent(context, VerifyActivity.class);
    }

    @ProvidePresenter
    VerifyPresenter presenter() {
        return appComponent().verifyComponentBuilder().build().presenter();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.verifyContinue)
    void onContinueClick() {
        presenter.onContinueClick();
    }

    @Override
    public void openMainScreen() {
        Intent intent = ImagesActivity.createIntent(this);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void emailNotVerified() {
        Toast.makeText(this, R.string.verify_email_not_verified, Toast.LENGTH_SHORT).show();
    }
}
