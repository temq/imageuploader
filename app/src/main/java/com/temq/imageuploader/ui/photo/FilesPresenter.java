package com.temq.imageuploader.ui.photo;

import android.Manifest;
import android.net.Uri;

import com.arellomobile.mvp.InjectViewState;
import com.google.firebase.FirebaseException;
import com.temq.imageuploader.BuildConfig;
import com.temq.imageuploader.model.FileItem;
import com.temq.imageuploader.network.AuthController;
import com.temq.imageuploader.network.FilesStorage;
import com.temq.imageuploader.ui.BasePresenter;
import com.temq.imageuploader.utils.AppSchedullers;
import com.temq.imageuploader.utils.PermissionChecker;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;

@InjectViewState
public class FilesPresenter extends BasePresenter<FileView> {

    private final FilesStorage filesStorage;
    private final AppSchedullers appSchedullers;
    private final AuthController authController;
    private final PermissionChecker permissionChecker;
    private List<FileItem> currentFiles;
    private FileItem fileForDownload;

    @Inject
    public FilesPresenter(FilesStorage filesStorage,
                          AppSchedullers appSchedullers,
                          AuthController authController,
                          PermissionChecker permissionChecker) {
        this.filesStorage = filesStorage;
        this.appSchedullers = appSchedullers;
        this.authController = authController;
        this.permissionChecker = permissionChecker;
        currentFiles = Collections.emptyList();
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        Disposable disposable = filesStorage.loadFiles()
                .subscribeOn(appSchedullers.network())
                .observeOn(appSchedullers.ui())
                .onErrorReturnItem(Collections.emptyList())
                .doOnSuccess(fileItems -> currentFiles = fileItems)
                .subscribe(fileItems -> getViewState().showFiles(fileItems));
        add(disposable);
    }

    public void onFileSelected(Uri uri) {
        Disposable disposable = filesStorage.uploadFile(uri)
                .subscribeOn(appSchedullers.network())
                .map(fileItem -> {
                    ArrayList<FileItem> newItems = new ArrayList<>(currentFiles.size() + 1);
                    newItems.addAll(currentFiles);
                    newItems.add(fileItem);
                    return newItems;
                })
                .observeOn(appSchedullers.ui())
                .subscribe(items -> {
                    currentFiles = items;
                    getViewState().showFiles(currentFiles);
                });
        add(disposable);
    }

    public void onSelectFileClick() {
        getViewState().openSelectFile();
    }

    public void logout() {
        Disposable disposable = authController.logout()
                .subscribeOn(appSchedullers.network())
                .observeOn(appSchedullers.ui())
                .subscribe(() -> getViewState().openAuthScreen(),
                        throwable -> {
                            if (BuildConfig.DEBUG) {
                                throwable.printStackTrace();
                            }

                            if (throwable instanceof FirebaseException) {
                                getViewState().showError(throwable.getMessage());
                            } else {
                                getViewState().showUnknownLogoutError();
                            }
                        });
        add(disposable);
    }

    public void startDownload(FileItem fileItem) {

        if (!permissionChecker.isGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            getViewState().requestPermission();
            return;
        }

        Disposable disposable = filesStorage.downloadFile(fileItem)
                .subscribeOn(appSchedullers.network())
                .observeOn(appSchedullers.ui())
                .subscribe(progress -> {

                        }, throwable -> getViewState().errorFileLoading(),
                        () -> getViewState().showLoadSuccess());
        add(disposable);
    }

    public void permissionGranted() {
        if (fileForDownload != null) {
            startDownload(fileForDownload);
            fileForDownload = null;
        }
    }
}
