package com.temq.imageuploader.ui.resetpass;

import dagger.Subcomponent;

@Subcomponent
public interface ResetPasswordComponent {

    ResetPasswordPresenter presenter();

    @Subcomponent.Builder
    interface Builder {
        ResetPasswordComponent build();
    }
}
