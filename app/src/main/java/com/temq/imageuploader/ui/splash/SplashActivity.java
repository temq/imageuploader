package com.temq.imageuploader.ui.splash;

import android.os.Bundle;

import com.temq.imageuploader.ui.BaseActivity;
import com.temq.imageuploader.ui.auth.AuthActivity;
import com.temq.imageuploader.ui.photo.ImagesActivity;
import com.temq.imageuploader.ui.verify.VerifyActivity;
import com.temq.imageuploader.utils.AccountHelper;

import javax.inject.Inject;

public class SplashActivity extends BaseActivity {

    @Inject
    AccountHelper accountHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appComponent().inject(this);

        if (accountHelper.isAuth()) {
            if (accountHelper.isEmailVerified()) {
                startActivity(ImagesActivity.createIntent(this));
            } else {
                startActivity(VerifyActivity.createIntent(this));
            }
        } else {
            startActivity(AuthActivity.createIntent(this));
        }
    }
}
