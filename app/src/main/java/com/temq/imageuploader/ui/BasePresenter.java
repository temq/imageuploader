package com.temq.imageuploader.ui;

import com.arellomobile.mvp.MvpPresenter;
import com.arellomobile.mvp.MvpView;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class BasePresenter<V extends MvpView> extends MvpPresenter<V> {

    private final CompositeDisposable disposables;


    public BasePresenter() {
        disposables = new CompositeDisposable();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        disposables.dispose();
    }

    protected void add(Disposable disposable) {
        disposables.add(disposable);
    }
}
